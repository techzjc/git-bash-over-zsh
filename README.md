# Git Bash over zsh

> zsh customization for the Git Bash
>
> (Modified from the GitHub Project [fabriziocucci/git-bash-for-mac](https://github.com/fabriziocucci/git-bash-for-mac))

## Installation

Paste the following code and run it in the **`Terminal`** app

```sh
/bin/zsh -c "$(curl -fsSL https://git.cs.vt.edu/techzjc/git-bash-over-zsh/-/raw/main/install.sh)"
```

Then restart the **`Terminal`** app, and you have successfully installed~

## Uninstallation

Paste the following code and run it in the **`Terminal`** app

```bash
/bin/bash -c "$(curl -fsSL https://git.cs.vt.edu/techzjc/git-bash-over-zsh/-/raw/main/uninstall.sh)"
```

Then restart the **`Terminal`** app, and you have successfully uninstalled.

